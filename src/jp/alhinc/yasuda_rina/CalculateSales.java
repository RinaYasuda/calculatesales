package jp.alhinc.yasuda_rina;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



public class CalculateSales {
	public static void main(String[] args) {
		//System.out.println("ここにあるファイルを開きます=>"+ args[0]);

		//Hashmap作成
		Map<String,String> branches = new HashMap<String,String>();
		Map<String,Long> profit = new HashMap<String,Long>();

		//branch lstのファイルを読み込む

		BufferedReader br = null;

		try {
				File file = new File(args[0],"branch.lst");

				if(!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;

			}
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) !=null){
				//支店コードと支店名を保持
					String[] names = line.split(",");
					branches.put(names[0],names[1]);
					profit.put(names[0],0L);

					//System.out.println(line);
	 				if (!names[0].matches("^[0-9]{3}$")) {
	 					System.out.println("支店定義ファイルのフォーマットが不正です");
	 					return;

	 				}if (names.length > 2) {
	 					System.out.println("支店定義ファイルのフォーマットが不正です");
	 					return;

	 				}
				}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {

	 		if(br != null) {
	 			try {
	 				br.close();

	 			} catch(IOException e) {
	 				return;

	 			}

			}

		}

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept (File file, String str) {
				if(str.matches("^[0-9]{8}.rcd$")) {
					return true;
				} else {
					return false;
				}
			}
		};

		File files = new File (args[0]);
		File[] profitFiles = files.listFiles(filter);


		BufferedReader brs = null;

		for(int i = 0; i < profitFiles.length; i++) {
			try {
			FileReader frs = new FileReader(profitFiles[i]);
			brs = new BufferedReader(frs);

			List<String> profitList = new ArrayList<>();
			String lines;
			while((lines = brs.readLine()) !=null){
				profitList.add(lines);
			}

			if(!(branches.containsKey(profitList.get(0)))) {
				System.out.println("<"+profitFiles[i]+"支店コードが不正です");
				return;

			} if(profitList.size() >= 3) {
				System.out.println("<"+profitFiles[i] + "のフォーマットが不正です");
				return;

			}
			Long data = Long.parseLong(profitList.get(1));
			Long sum = profit.get(profitList.get(0));
				if(sum == null) {
					profit.put(profitList.get(0), data);

			}else {
				sum +=data;
				//System.out.println(sum);

			} if(String.valueOf(sum).length() > 10){
				System.out.println("合計金額が10桁を超えました");
				return;

			} profit.put(profitList.get(0), sum);

			} catch(IOException e1) {
				System.out.println("予期せぬエラーが発生しました");
				return;

					} finally {
						if(brs != null) {
							try {
								brs.close();

								} catch(IOException e1) {
								}
						}
					}
		}

		BufferedWriter bw =null;

			try {
				  File fileOut = new File(args[0],"branch.out");
				  FileWriter fw = new FileWriter(fileOut);
				  bw = new BufferedWriter(fw);


				  for (String key : profit.keySet()) {

					  bw.write(key + ",");
					  bw.write(branches.get(key) + ",");
					  String line = Long.toString(profit.get(key));
					  bw.write(line);
					  bw.newLine();



					 } bw.close();

			} catch(IOException e1) {
				System.out.println("予期せぬエラーが発生しました");
				//System.out.println(e);
				return;
			}

	}
}


